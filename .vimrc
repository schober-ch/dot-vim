" vimrc with many things from http://dougblack.io/words/a-good-vimrc.html


" vundle setup
set nocompatible              " be iMproved, required
filetype off                  " required
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'LaTeX-Box-Team/Latex-Box'
Plugin 'sjl/gundo.vim'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'kien/ctrlp.vim'
Plugin 'rking/ag.vim'
"Plugin 'chriskempson/base16-vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'klen/python-mode'
Plugin 'heavenshell/vim-pydocstring'
"Plugin 'vale1410/vim-minizinc'
"Plugin 'vim-pandoc/vim-pandoc'
"Plugin 'vim-pandoc/vim-pandoc-syntax'
"Plugin 'othree/html5.vim'
"Plugin 'JulesWang/css.vim'
"Plugin 'sukima/xmledit'
"Plugin 'https://github.com/gorodinskiy/vim-coloresque.git'
"Plugin 'ap/vim-css-color'
"Plugin 'pangloss/vim-javascript'
"Plugin 'nvie/vim-pyunit'

call vundle#end()            " required
filetype plugin indent on    " required

" colorscheme
set background=dark
colorscheme solarized
"" support for 256 colors in screen
"if &term =~# '^\(screen\|xterm\)$'
"    set t_Co=256
"endif
"colorscheme base16-solarized
"let g:solarized_termcolors=256

" misc
let mapleader=","       " leader is comma
syntax enable           " enable syntax processing

" spaces and tabs
set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set expandtab       " tabs are spaces


" Simplify saving and closing of files
map <F8> :w<CR>
map <F10> :x<CR>
map <F9> :set spell!<CR>
let g:tex_comment_nospell=1


" Programming stuff
" Fortran
let fortran_free_source=1
let fortran_fold=1

" Python
let g:pymode_rope_complete_on_dot = 0 " deactivate autocomplete while writing.

" UI layout
set number              " show line numbers
set showcmd             " show command in bottom bar
set cursorline          " highlight current line
filetype indent on      " load filetype-specific indent files
set wildmenu            " visual autocomplete for command menu
set lazyredraw          " redraw only when we need to.
set showmatch           " highlight matching [{()}]

" Searching
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>

" folding
set foldenable          " enable folding
set foldlevelstart=10   " open most folds by default
set foldnestmax=10      " 10 nested fold max
" space open/closes folds
nnoremap <space> za
set foldmethod=indent   " fold based on indent level
" highlight last inserted text
nnoremap gV `[v`]

" line shortcuts
" undo for rope
nnoremap <C-c>uu :PymodeRopeUndo<CR>  
" leader shortcuts
"inoremap jk <esc>
" toggle gundo
nnoremap <leader>u :GundoToggle<CR>
" edit vimrc/zshrc and load vimrc bindings
nnoremap <leader>ev :vsp $MYVIMRC<CR>
nnoremap <leader>ez :vsp ~/.bashrc<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>
" save session
nnoremap <leader>s :mksession<CR>
" open ag.vim
nnoremap <leader>a :Ag

" latex
let g:LatexBox_latexmk_async = 0
let g:LatexBox_latexmk_preview_continuously = 1
" powerline

" CtrlP settings
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'
let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

" launch config

" autogroups
augroup configgroup
    autocmd!
    autocmd VimEnter * highlight clear SignColumn
"    autocmd BufWritePre *.php,*.py,*.js,*.txt,*.hs,*.java,*.md
"                \:call <SID>StripTrailingWhitespaces()
    autocmd FileType java setlocal noexpandtab
    autocmd FileType java setlocal list
    autocmd FileType java setlocal listchars=tab:+\ ,eol:-
    autocmd FileType java setlocal formatprg=par\ -w80\ -T4
    autocmd FileType php setlocal expandtab
    autocmd FileType php setlocal list
    autocmd FileType php setlocal listchars=tab:+\ ,eol:-
    autocmd FileType php setlocal formatprg=par\ -w80\ -T4
    autocmd FileType ruby setlocal tabstop=2
    autocmd FileType ruby setlocal shiftwidth=2
    autocmd FileType ruby setlocal softtabstop=2
    autocmd FileType ruby setlocal commentstring=#\ %s
    autocmd FileType html setlocal shiftwidth=4
    autocmd FileType python setlocal commentstring=#\ %s
    autocmd BufEnter *.cls setlocal filetype=java
    autocmd BufEnter *.zsh-theme setlocal filetype=zsh
    autocmd BufEnter Makefile setlocal noexpandtab
    autocmd BufEnter *.sh setlocal tabstop=2
    autocmd BufEnter *.sh setlocal shiftwidth=2
    autocmd BufEnter *.sh setlocal softtabstop=2
augroup END

" backups

" custom stuff

" toggle between number and relativenumber
function! ToggleNumber()
    if(&relativenumber == 1)
        set norelativenumber
        set number
    else
        set relativenumber
    endif
endfunc
nnoremap <leader>t :call ToggleNumber()<CR>

" function! ToggleSpell()
"     if(&set_spell == 1)
"         set nospell
"     else
"         set spell
"     endif
" endfunc
" nnoremap <F9> :call ToggleSpell()<CR>

" strips trailing whitespace at the end of files. this
" is called on buffer write in the autogroup above.
" function! <SID>StripTrailingWhitespaces()
"     " save last search & cursor position
"     let _s=@/
"     let l = line(".")
"     let c = col(".")
"     %s/\s\+$//e
"     let @/=_s
"     call cursor(l, c)
" endfunction
